const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const connectedClients = [];

// Data file to keep the code DRY
const data = require('./data.json');

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

/**
 * Scince we dont have the actual data here,
 * use this function to generate random data and send it to connected clients
 *
 * @param {Array} arr Pass data array
 * @returns {Object} random data object from given array
 */
function getRandomData(arr) {
  const randomNumber = Math.floor(Math.random() * Math.floor(5));
  return arr.filter(el => el.clientId == randomNumber);
}

/**
 * this function checks if the socket id exists in connected clients.
 *
 * @param {String} socketId socket id to find
 * @returns {String} Returns the index of the first element in the array where predicate is true, and -1 otherwise.
 */
function ClientExists(socketId) {
  // return connectedClients.some(e => e.socket_id === socketId);
  return connectedClients.findIndex(socket => socket.socket_id == socketId);
}

// You can give a Socket.io server arbitrary functions via io.use() that are run when a socket is created.
// we are using it to authenticate connection
io.use(function(socket, next) {
  // Get handshake data
  const handshakeData = socket.handshake.query;
  const userName = handshakeData.username;
  const userPassword = handshakeData.password;
  const client = handshakeData.clientId;

  // verify login credentials. this is only for demostration purposes. obviously we won't hard-code login credentials and verify these like this.
  // TODO: Use a JWT (https://www.npmjs.com/package/socketio-jwt) for authentication
  if (userName === 'demo' && userPassword === 'demo') {
    // if this client is not already stored.
    if (ClientExists(socket.id) == '-1') {
      // store client information
      connectedClients.push({ clientId: client, socket_id: socket.id });
    }

    // All good. lets proceed next.
    next();
  } else {
    // Auth failed. send error
    next(
      new Error(
        "{ 'code': 401, 'message': 'Authentication Error, please try again'}"
      )
    );
  }
});

// When a new socket connection is established
io.on('connection', function(socket) {
  // Send auth feedback
  socket.emit('message', 'Authentication successful, welcome');

  // Send Random Data to all connected socket clients every 5 seconds
  setInterval(function() {
    // get random data object 3,4 objects of same clientID
    const message = getRandomData(data);

    // get above objects' clientId
    const client = message[0].clientId;

    // get a all instences of a client
    const clientinstances = connectedClients.filter(
      el => el.clientId == client
    );

    // Loop through all messages and then send them to their clients in round-robin fashion.
    let loopCount = 0;
    const totalInstances = clientinstances.length;
    message.forEach(function(el) {
      if (loopCount > totalInstances) loopCount = 0;
      const clientSocket = clientinstances[loopCount];
      if (clientSocket) {
        io.to(clientSocket.socket_id).emit('message', el);
      }
      loopCount++;
    });

    console.log(clientinstances);
  }, 5000);

  // reomve client info on disconect
  socket.on('disconnect', function() {
    connectedClients.splice(
      connectedClients.findIndex(function(el) {
        return el.socket_id === socket.id;
      }),
      1
    );
  });
});

http.listen(3000, function() {
  console.log('listening on *:3000');
});
